# Temperatur Sensor am Raspberry Pi

*In diesem Projekt geht es um den Betrieb eines LM75 Temperatursensors an einem Raspberry Pi 3.*


Der physische Aufbau des Projektes liegt in Form eines Fritzing-Plans vor und befindet sich ebenfalls in Bildform unter dem Punkt *Hardware* in dieser Dokumentation.

## Software
Die Software zur Auswertung der Temperatur liegt in Form eines Python-Scripts und eines Java-Programms vor.
Weitere Details zu beiden Varianten folgen, wenn ich später Lust dazu habe :3

## Hardware
Der Aufbau des Pi's, Sensors und der LED sieht folgendermaßen aus:
![Aufbau](./docs/breadboard.png)