import time
import smbus
import threading
import RPi.GPIO as GPIO

# Konstanten definieren
I2C_TEMP_LM75_ADRESS = 0x48             # I2C-Adresse des LM75 Temperatursensors (0x48)
LM75_TEMP_BYTE = 0x00                   # Adresse im LM75, ab der Daten ausgelesen werden
LED_GPIO_ADDRESS = 18                   # GPIO Port der anzusteuernden LED
TEMP_THRESHOLD = 20                     # Temperatur ab welcher die LED aktiviert wird
QUERY_DELAY = 1                         # Wartezeit zwischen Temperaturabfragen

# I2C-Bus initialisieren
i2c = smbus.SMBus(1)

# GPIO initialisieren
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(LED_GPIO_ADDRESS, GPIO.OUT)


def getTemp():
    try:
        raw = i2c.read_i2c_block_data(I2C_TEMP_LM75_ADRESS, LM75_TEMP_BYTE)
        temp = (((raw[0] << 8) | raw[1]) >>7) * 0.5
        return temp if temp <= 125 else temp - 256
    except:
        print('Could not read temperature. Please check LM75 connection!')


def flash_led():
    while not getattr(threading.currentThread(), 'kill', False):
        if getattr(threading.currentThread(), 'active', False):
            GPIO.output(LED_GPIO_ADDRESS, GPIO.HIGH)
            time.sleep(0.25)
            GPIO.output(LED_GPIO_ADDRESS, GPIO.LOW)
            time.sleep(0.25)


if __name__ == '__main__':
    led_thread = threading.Thread(target=flash_led)
    led_thread.start()

    try:
        while True:
            temp = getTemp()
            print('[' + time.strftime('%H:%M:%S') + '] ' +  'Temperatur: ' + str(temp) + ' °C')
            led_thread.active = temp > TEMP_THRESHOLD
            time.sleep(QUERY_DELAY)
    except KeyboardInterrupt:
        print('[' + time.strftime('%H:%M:%S') + '] ' +  'Exiting!')
        led_thread.kill = True
        GPIO.output(LED_GPIO_ADDRESS, GPIO.LOW)